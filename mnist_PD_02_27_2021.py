#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = ["Griselda García Bartolo","E. Ulises Moya", "Abraham Sanchez"]
__copyright__ = "Copyright 2020, Universidad Autónoma de Guadalajara"
__credits__ = ["Copyright 2019, The TensorFlow Authors."]
__Web_credits__ =["https://colab.research.google.com/github/tensorflow/privacy/blob/master/tutorials/Classification_Privacy.ipynb?ref=hackernoon.com&pli=1#scrollTo=rfIZyGCOscFp"]
__license__ = "Licensed under the Apache License, Version 2.0 (the License)"
__version__ = "1.15.2"
__maintainer__ = ["Griselda García Bartolo"]
__email__ = "griselda.garcia@edu.uag.mx and gris_gab@hotmail.com"
__status__ = "Development"

import tensorflow as tf

import numpy as np
import argparse
import csv
from absl import app
from absl import flags
from absl import logging
import pandas as pd      
import seaborn as sn
import numpy as np
import tensorflow as tf
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix
import pandas as pd
import time

begin = time.time()

from tensorflow_privacy.privacy.analysis import compute_dp_sgd_privacy
from tensorflow_privacy.privacy.optimizers.dp_optimizer import DPGradientDescentGaussianOptimizer
from tensorflow_privacy.privacy.optimizers.dp_optimizer import DPGradientDescentOptimizer
from tensorflow_privacy.privacy.analysis.rdp_accountant import get_privacy_spent
from tensorflow_privacy.privacy.analysis.rdp_accountant import compute_rdp

print(tf.__version__)


#input parameters
parser = argparse.ArgumentParser(description='DP train mnist')
parser.add_argument('-b', '--batchsize', required=True, type=int, help='Batch size')
parser.add_argument('-e', '--epochs', required=True, type=int, help='Epochs')
parser.add_argument('-l', '--eta', required=True, type=float, help='Learning rate')
parser.add_argument('-l2', '--norma', required=True, type=float, help='l2_norm_clip')
parser.add_argument('-nm', '--note2', required=True, type=float, help='noise_multiplier')
parser.add_argument('-j', '--jobid', required=True, type=int, help='jobid')
parser.add_argument('-m', '--model1', required=True, type=int, help='model1')
#print txt

args = parser.parse_args()
#Actualize delta 1/50000
delta= 2e-5
batch_size = args.batchsize
epochs = args.epochs
learning_rate = args.eta
l2_norm_clip = args.norma
noise_multiplier = args.note2
jobid = args.jobid
model1 = args.model1
print('jobid', jobid)
print('delta',delta)
num_microbatches = batch_size

if batch_size % num_microbatches != 0:
 raise ValueError('Batch size should be an integer multiple of the number of microbatches')
#---------------------------

#Define plot
def plot_acc(history):
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    
def plot_loss(history):
    # summarize history for loss
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')

(x, y), (x_test, y_test) = tf.keras.datasets.mnist.load_data()

#---------------------------The data, split between train and test sets
x_train=x[0:50000]
y_train=y[0:50000]
x_val=x[50000:60000]
y_val=y[50000:60000]
#train, test = tf.keras.datasets.mnist.load_data()
train_data = x_train
train_labels = y_train
test_data = x_test
test_labels = y_test
val_data = x_val
val_labels = y_val

train_data = np.array(train_data, dtype=np.float32) / 255
test_data = np.array(test_data, dtype=np.float32) / 255
val_data = np.array(val_data, dtype=np.float32) / 255

train_data = train_data.reshape(train_data.shape[0], 28, 28, 1)
test_data = test_data.reshape(test_data.shape[0], 28, 28, 1)
val_data = val_data.reshape(val_data.shape[0], 28, 28, 1)

train_labels = np.array(train_labels, dtype=np.int32)
test_labels = np.array(test_labels, dtype=np.int32)
val_labels = np.array(val_labels, dtype=np.int32)

train_labels = tf.keras.utils.to_categorical(train_labels, num_classes=10)
test_labels = tf.keras.utils.to_categorical(test_labels, num_classes=10)
val_labels = tf.keras.utils.to_categorical(val_labels, num_classes=10)

assert train_data.min() == 0.
assert train_data.max() == 1.
assert test_data.min() == 0.
assert test_data.max() == 1.
assert val_data.min() == 0.
assert val_data.max() == 1.


#-------------Define Model
model = tf.keras.Sequential([
    tf.keras.layers.Conv2D(16, 8,
                           strides=2,
                           padding='same',
                           activation='relu',
                           input_shape=(28, 28, 1)),
    tf.keras.layers.MaxPool2D(2, 1),
    tf.keras.layers.Conv2D(32, 4,
                           strides=2,
                           padding='valid',
                           activation='relu'),
    tf.keras.layers.MaxPool2D(2, 1),
    tf.keras.layers.Flatten(),
    tf.keras.layers.Dense(32, activation='relu'),
    tf.keras.layers.Dense(10, activation='softmax')
])

model.summary()
#----------------Define Optimizer
optimizer = DPGradientDescentGaussianOptimizer(
    l2_norm_clip=l2_norm_clip,
    noise_multiplier=noise_multiplier,
    num_microbatches=num_microbatches,
    learning_rate=learning_rate)

loss = tf.keras.losses.CategoricalCrossentropy(
    from_logits=True, reduction=tf.losses.Reduction.NONE)

#----------Compile Model
model.compile(optimizer=optimizer, loss=loss, metrics=['accuracy'])

history1 = model.fit(train_data, train_labels,
          epochs=epochs,
          validation_data=(val_data, val_labels),
          batch_size=batch_size)

#--------------------Validation Accuracy
print("Max. Validation Accuracy: {}%".format(round(100*max(history1.history['val_acc']), 2)))
max_val_acc = round(100*max(history1.history['val_acc']), 2)

#--------------------TrainAccuracy
max_train_acc = round(100*max(history1.history['acc']), 2)

#--------------------Compute the value of epsilon, the measure of privacy
alfa=compute_dp_sgd_privacy.compute_dp_sgd_privacy(n=x_train.shape[0], batch_size=batch_size, noise_multiplier=noise_multiplier,
epochs=epochs, delta=delta)

eps,rdp=alfa
eps=np.round(eps,3)
print(eps,rdp)

hist = pd.DataFrame(history1.history)
df = hist.to_csv(pathcsv+'hist_mnist_{}_{}_{}_{}_{}_{}_{}_{}_{}_{}.csv'.format(model1,jobid, batch_size,epochs,learning_rate,l2_norm_clip, noise_multiplier,
 eps,rdp,delta))
 
#--------------------Save figures
fig = plt.figure(figsize=(15, 5))
plt.subplot(1, 2, 1)
acc_train = history1.history['acc']
acc_val = history1.history['val_acc']
epocas = range(1,len(acc_val)+1)
plt.plot(epocas, acc_train, 'c', label='Train')
plt.plot(epocas, acc_val, 'm', label='Test')
plt.title('Model Accuracy')
plt.xlabel('Epochs')
plt.ylabel('Accuracy')
#plt.legend()

plt.subplot(1, 2, 2)
loss_train = history1.history['loss']
loss_val = history1.history['val_loss']
epocas = range(1,len(loss_val)+1)
plt.plot(epocas, loss_train, 'c', label='Train')
plt.plot(epocas, loss_val, 'm', label='Test')
plt.title('Model Loss')
plt.xlabel('Epochs')
plt.ylabel('Loss')
plt.savefig(pathimg+'plots_imag_{}_{}_{}_{}_{}_{}_{}_{}_{}_{}.png'.format(model1,jobid, batch_size,epochs,learning_rate,l2_norm_clip, noise_multiplier,
eps,rdp,delta))

#--------------------Predict
pred = model.predict(test_data, batch_size = batch_size, verbose=1)
predicted = np.argmax(pred, axis = 1)

a = np.argmax(test_labels, axis=1)
print(predicted)
print(a.shape)
print (a)
print(predicted.shape)

#--------------------Report
from sklearn.metrics import confusion_matrix, classification_report
cnn_report = classification_report(np.argmax(test_labels, axis = 1), predicted,output_dict=True)
#print(cnn_report.dtype)
for key in cnn_report: 
    if key == 'accuracy':
      print(key, ' : ', cnn_report[key])
      max_test_acc = cnn_report[key]
max_test_acc = round(100*max_test_acc, 2)
print(max_test_acc)

#--------------------Time
end = time.time()
lapse = np.round(end - begin,0)
print(lapse)

#--------------------Confusion Matrix
mf = confusion_matrix (a, predicted)
print(mf)
plt.figure(figsize=(10,8))
heat_map =sn.heatmap(mf, cmap="YlGnBu", annot=True)

plt.savefig(pathimg+'plots_mnist_{}_{}_{}_{}_{}_{}_{}_{}_{}_{}.png'.format(model1,jobid, batch_size,epochs,learning_rate,l2_norm_clip, noise_multiplier,
eps,rdp,delta))

row_0=[model1,jobid, batch_size,epochs,learning_rate,l2_norm_clip, noise_multiplier,
 eps,rdp,delta, max_val_acc, max_train_acc, max_test_acc,lapse ]

with open(pathcsv+'mnistresults.csv','a') as f:
    #writer = csv.writer(f, delimiter=",",lineterminator='\n')
    writer = csv.writer(f, delimiter=',',lineterminator='\n')
    writer.writerow(row_0)
    f.close()


